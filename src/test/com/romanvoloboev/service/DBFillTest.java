package com.romanvoloboev.service;

import com.romanvoloboev.entity.*;
import com.romanvoloboev.model.ApplicantModel;
import com.romanvoloboev.model.ExamResultModel;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Roman Voloboev
 */
@ContextConfiguration(locations = {"classpath:spring-config.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class DBFillTest {

    @Autowired private SubjectBOImpl subjectBO;
    @Autowired private ApplicantBOImpl applicantBO;
    @Autowired private DepartmentBOImpl departmentBO;
    @Autowired private SpecialityBOImpl specialityBO;

    @Test
    public void fillSubject() throws Exception {
        String[] name = {"Математика", "Укр. яз."};
        for (String aName : name) {
            subjectBO.save(new Subject(aName));
        }
    }

    @Test
    public void fillDepartment() throws Exception {
        String[] name = {"Транспортные технологии", "Электромеханика", "Радиотехника", "Компьютерная и программная инженерия", "Воздушный транспорт"};
        for (String aName : name) {
            departmentBO.save(new Department(aName));
        }
    }

    @Test
    public void fillSpeciality() throws Exception {
        String[] name = {"Организация авиационных перевозок", "Эксплуатация авиационных комплексов", "Техническая эксплуатация радиоэлектронного оборудования ВС",
                "Техническая эксплуатация наземных средств радиоэлектронного обеспечения полетов", "Разработка ПО", "Обслуживание компьютерных систем и сетей",
                "Техническое обслуживание ВС и двигателей", "Техническое обслуживание средств хранения, транспортировки и заправки ГСМ"};
        Integer[] department = {1, 2, 3, 3, 4, 4, 5, 5};

        for (int i=0; i<name.length; i++) {
            specialityBO.save(new Speciality(name[i], departmentBO.selectEntityById(department[i]), 4.5));
        }
    }

    @Test
    public void fillApplicant() throws Exception {
        applicantBO.save(new Applicant("Иванов И.И.", "М", applicantBO.formatDate("2014-10-21"), "11-22-33",
                "ул. Трубников, 11", "ЗМ", applicantBO.formatDate("2014-10-21"), "КЗНСЗШ №7", "ул. Харитонова, 2", "12-11-22", specialityBO.selectEntityById(4)));
        applicantBO.save(new Applicant("Петрова М.А.", "Ж", applicantBO.formatDate("2014-10-21"), "11-22-33",
                "ул. Трубников, 11", "ЗМ", applicantBO.formatDate("2014-10-21"), "КЗНСЗШ №7", "ул. Харитонова, 2", "12-11-22", specialityBO.selectEntityById(1)));
        applicantBO.save(new Applicant("Сидоров К.Л.", "М", applicantBO.formatDate("2014-10-21"), "11-22-33",
                "ул. Трубников, 11", "ЗМ", applicantBO.formatDate("2014-10-21"), "КЗНСЗШ №7", "ул. Харитонова, 2", "12-11-22", specialityBO.selectEntityById(2)));
    }
}
