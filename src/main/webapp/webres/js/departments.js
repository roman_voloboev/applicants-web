function loadDepartmentsData() {
    $("#department-table").find("tr:gt(0)").remove();
    $.post("/departments/load",

        function(responce) {
            for(var i=0;i<responce.length;i++) {
                appendData(responce[i].id, responce[i].name);
            }
        });
}
function appendData(id, name) {
    $('#department-table').append('<tr><td>' + id + '</td><td>' + name + '</td>' +
        '<td class="text-center">' +
        '<a style="margin: 2px;" class="btn btn-danger btn-xs" href="/departments/delete?id='+id+'"><span style="top: 0;" class="glyphicon glyphicon-remove"></span></a>'+
        '</td></tr>');
}


function saveDep() {
    var depData = $('#departmentForm').serialize();
    $.ajax({
        data: depData,
        url: '/departments/save',
        type: 'post',
        dataType: 'json',
        success: function(responce){
        }
    });
}