function loadSpecData() {
    $("#speciality-table").find("tr:gt(0)").remove();
    $.post("/specialities/load",
        function(responce) {
            for(var i=0;i<responce.length;i++) {
                appendData(responce[i].id, responce[i].name, responce[i].department, responce[i].averageMark);
            }
        });
}
function appendData(id, name, department, averageMark) {
    $('#speciality-table').append('<tr><td>' + id + '</td><td>' + name + '</td><td>' + department + '</td><td>' + averageMark + '</td>' +
        '<td class="text-center">' +
        '<a style="margin: 2px;" class="btn btn-danger btn-xs" href="/specialities/delete?id='+id+'"><span style="top: 0;" class="glyphicon glyphicon-remove"></span></a>'+
        '</td></tr>');
}

function loadDepData() {
    $.post("/departments/load",
        function(responce){
            $('#department').html('<option selected disabled>Выберите из списка...</option>');
            for(var i=0;i<responce.length;i++){
                $('#department').append('<option value="'+responce[i].id+'">'+responce[i].name+'</option>');
            }
        });
}

function saveSpec() {
    var specData = $('#specialityForm').serialize();
    $.ajax({
        data: specData,
        url: '/specialities/save',
        type: 'post',
        dataType: 'json',
        success: function(responce){
        }
    });
}