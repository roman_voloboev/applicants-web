function loadSubjectsData() {
    $("#subject-table").find("tr:gt(0)").remove();
    $.post("/subjects/load",

        function(responce) {
            for(var i=0;i<responce.length;i++) {
                appendSubjectsData(responce[i].id, responce[i].name);
            }
        });
}
function appendSubjectsData(id, name) {
    $('#subject-table').append('<tr><td>' + id + '</td><td>' + name + '</td>' +
        '<td class="text-center">' +
        '<a style="margin: 2px;" class="btn btn-danger btn-xs" href="/subjects/delete?id='+id+'"><span style="top: 0;" class="glyphicon glyphicon-remove"></span></a>'+
        '</td></tr>');
}


function saveSubj() {
    var subjectData = $('#subjectForm').serialize();
    $.ajax({
        data: subjectData,
        url: '/subjects/save',
        type: 'post',
        dataType: 'json',
        success: function(responce){
        }
    });
}