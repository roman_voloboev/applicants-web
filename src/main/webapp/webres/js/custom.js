(function(){
    'use strict';
    var $ = jQuery;
    $.fn.extend({
        filterTable: function(){
            return this.each(function(){
                $(this).on('keyup', function(e){
                    $('.filterTable_no_results').remove();
                });
            });
        }
    });
    $('[data-action="search"]').filterTable();
})(jQuery);

$(function(){
    $('[data-action="search"]').filterTable();

    $('.container').on('click', '.panel-heading span.search', function(e){
        var $this = $(this),
            $panel = $this.parents('.panel');

        $panel.find('.panel-body').slideToggle();

        if($panel.find('.panel-add').css('display') != 'none') {
            $panel.find('.panel-add').slideToggle();
        }

        if($this.css('display') != 'none') {
            $panel.find('.panel-body input').focus();
        }
    });
    $('[data-toggle="tooltip_search"]').tooltip();
});

$(function(){
    $('.container').on('click', '.panel-heading span.add', function(e){
        var $this = $(this),
            $panel = $this.parents('.panel');

        $panel.find('.panel-add').slideToggle();

        if($panel.find('.panel-body').css('display') != 'none') {
            $panel.find('.panel-body').slideToggle();
        }

        if($this.css('display') != 'none') {
            $panel.find('.panel-add input#name').focus();
        }
    });
});
