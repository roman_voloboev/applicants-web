function loadApplicantsData() {
    $("#applicants-table").find("tr:gt(0)").remove();
    $.post("/applicants/load",
        function(responce) {
           for(var i=0;i<responce.length;i++) {
                appendApplicantData(responce[i].id, responce[i].name, responce[i].sex, responce[i].birthday,
                    responce[i].phone, responce[i].address, responce[i].honours, responce[i].graduationDate,
                    responce[i].institutionName, responce[i].institutionAddress, responce[i].institutionPhone, responce[i].speciality);
            }
        });
}

function sortByHon() {
    $("#applicants-table > tbody").empty();
    $.post("/applicants/sort?by=honours",
        function(responce) {
            for(var i=0;i<responce.length;i++) {
                appendApplicantData(responce[i].id, responce[i].name, responce[i].sex, responce[i].birthday,
                    responce[i].phone, responce[i].address, responce[i].honours, responce[i].graduationDate,
                    responce[i].institutionName, responce[i].institutionAddress, responce[i].institutionPhone, responce[i].speciality);
            }
        });
}

function sortBySpec() {
    $("#applicants-table > tbody").empty();
    $.post("/applicants/sort?by=spec",
        function(responce) {
            for(var i=0;i<responce.length;i++) {
                appendApplicantData(responce[i].id, responce[i].name, responce[i].sex, responce[i].birthday,
                    responce[i].phone, responce[i].address, responce[i].honours, responce[i].graduationDate,
                    responce[i].institutionName, responce[i].institutionAddress, responce[i].institutionPhone, responce[i].speciality);
            }
        });
}


function searchByName() {
    $("#applicants-table > tbody").empty();
    $.post("/applicants/search", {name: $('#applicants-table-search').val()},
        function(responce) {
            if(responce.length > 0) {
                for(var i=0;i<responce.length;i++) {
                    appendApplicantData(responce[i].id, responce[i].name, responce[i].sex, responce[i].birthday,
                        responce[i].phone, responce[i].address, responce[i].honours, responce[i].graduationDate,
                        responce[i].institutionName, responce[i].institutionAddress, responce[i].institutionPhone, responce[i].speciality);
                }
            }else{
                $('#applicants-table').append('<tr><td colspan="12">Ничего не найдено</td></tr>');
            }
        });
}


function loadDepData() {
    $.post("/departments/load",
        function(responce){
            $('#department').html('<option selected disabled>Выберите из списка...</option>');
            for(var i=0;i<responce.length;i++){
                $('#department').append('<option value="'+responce[i].id+'">'+responce[i].name+'</option>');
            }
        });
}

function loadSpec() {
    $("#speciality").prop("disabled", false);
    id = $('#department option:selected').val();

    $.post("/specialities/load_by_dep", {id:id},
        function(responce){
            $('#speciality').html('<option selected disabled>Выберите из списка...</option>');
            for(var i=0;i<responce.length;i++){
                $('#speciality').append('<option value="'+responce[i].id+'">'+responce[i].name+'</option>');
            }
        });
}
function appendApplicantData(id, name, sex, birthday, phone, address, honours, graduationDate, institutionName, institutionAddress, institutionPhone, speciality) {
    $('#applicants-table').append('<tr><td>' + id + '</td><td>' + name + '</td>' +
        '<td>' + sex + '</td><td>' + birthday + '</td><td>' +phone + '</td>' +
        '<td>' + address + '</td><td>' + speciality + '</td><td>' + institutionName +
        '</td><td>' + institutionAddress + '</td><td>' + institutionPhone +
        '</td><td>' + graduationDate + '</td><td>' + honours + '</td>'+
        '<td class="text-center">' +
        '<a style="margin: 2px;" class="btn btn-info btn-xs no-print" href="/applicants/edit?id='+id+'"><span style="top: 0;" class="glyphicon glyphicon-pencil"></span></a>'+
        '<a style="margin: 2px;" class="btn btn-danger btn-xs no-print" href="/applicants/delete?id='+id+'"><span style="top: 0;" class="glyphicon glyphicon-remove"></span></a>'+
        '</td></tr>');
}


function saveApplicant() {
    var applicantsData = $('#applicantForm').serialize();
    $.ajax({
        data: applicantsData,
        url: '/applicants/save',
        type: 'post',
        dataType: 'json',
        success: function(responce){
        }
    });
}