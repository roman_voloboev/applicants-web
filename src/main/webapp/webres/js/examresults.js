function loadExamsResultsData() {
    $("#examresults-table").find("tr:gt(0)").remove();
    $.post("/examsresults/load",

        function(responce) {
            for(var i=0;i<responce.length;i++) {
                appendExamsResultData(responce[i].id, responce[i].applicant, responce[i].subject, responce[i].mark);
            }
        });
}
function appendExamsResultData(id, applicant, subject, mark) {
    $('#examresults-table').append('<tr><td>' + id + '</td><td>' + applicant + '</td>' +
        '<td>' + subject + '</td><td>' + mark + '</td>' +
        '<td class="text-center">' +
        '<a style="margin: 2px;" class="btn btn-danger btn-xs" href="/examsresults/delete?id='+id+'"><span style="top: 0;" class="glyphicon glyphicon-remove"></span></a>'+
        '</td></tr>');
}

function loadApplicants() {
    $.post("/applicants/load",
        function(responce){
            $('#applicant').html('<option selected disabled>Выберите из списка...</option>');
            for(var i=0;i<responce.length;i++){
                $('#applicant').append('<option value="'+responce[i].id+'">'+responce[i].name+'</option>');
            }
        });
}

function loadSubjects() {
    $.post("/subjects/load",
        function(responce){
            $('#subject').html('<option selected disabled>Выберите из списка...</option>');
            for(var i=0;i<responce.length;i++){
                $('#subject').append('<option value="'+responce[i].id+'">'+responce[i].name+'</option>');
            }
        });
}

function loadAvgMarks() {
    $("#avgmarks-table > tbody").empty();
    $.post("/examsresults/load_avg_marks",
        function(responce) {
            for(var i=0;i<responce.length;i++) {
                if(responce[i].avgApplicantMark >= responce[i].avgSpecMark) {
                    appendAvgRes(responce[i].name, responce[i].avgApplicantMark, "Да");
                }
                else {
                    appendAvgRes(responce[i].name, responce[i].avgApplicantMark, "Нет");
                }
            }
        });
}

function appendAvgRes(name, avgApplicantMark, result) {
    $('#avgmarks-table').append('<tr><td>' + name + '</td><td>' + avgApplicantMark + '</td><td>' + result + '</td></tr>');
}

function saveExamResult() {
    var examsResultsData = $('#examResultForm').serialize();
    $.ajax({
        data: examsResultsData,
        url: '/examsresults/save',
        type: 'post',
        dataType: 'json',
        success: function(responce){
        }
    });
}