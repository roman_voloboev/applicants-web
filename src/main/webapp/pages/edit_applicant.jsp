<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Аббитуриенты</title>
    <%@include file="include/head.jsp"%>
    <script type="text/javascript" src="../webres/js/index.js"></script>
</head>
<body>
<div class="mainmenu-wrapper">
    <div class="container">
        <nav class="mainmenu">
            <ul>
                <li class="active">
                    <a href='<c:url value="/applicants"/>'>Абитуриенты </a>
                </li>
                <li>
                    <a href='<c:url value="/examsresults"/>'>Результаты экзаменов</a>
                </li>
                <li>
                    <a href='<c:url value="/subjects"/>'>Предметы</a>
                </li>
                <li>
                    <a href='<c:url value="/specialities"/>'>Специальности</a>
                </li>
                <li>
                    <a href='<c:url value="/departments"/>'>Отделения</a>
                </li>
            </ul>
        </nav>
    </div>
</div>

<div class="container" style="margin-top: 15px;">
    <div class="col-md-12">
        <form:form modelAttribute="applicantModel" id="applicantForm" cssClass="form-horizontal">
            <div class="form-group">
                <div class="col-md-2">
                    <label for="name" class="control-label pull-right">ФИО</label>
                </div>
                <div class="col-md-8">
                    <form:hidden path="id"/>
                    <form:input path="name" id="name" cssClass="form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label for="birthday" class="col-md-2 control-label">Дата рождения</label>
                <div class="col-md-3">
                    <form:input path="birthday" id="birthday" type="date" cssClass="form-control"/>
                </div>

                <label for="sex" class="col-md-2 control-label">Пол</label>
                <div class="col-md-3">
                    <form:select path="sex" id="sex" cssClass="form-control">
                        <c:choose>
                            <c:when test="${applicantModel.sex == 'М'}">
                                <form:option value="М" selected="selected">М</form:option>
                                <form:option value="Ж">Ж</form:option>
                            </c:when>
                            <c:otherwise>
                                <form:option value="М">М</form:option>
                                <form:option value="Ж" selected="selected">Ж</form:option>
                            </c:otherwise>
                        </c:choose>
                    </form:select>
                </div>
            </div>

            <div class="form-group">
                <label for="phone" class="col-md-2 control-label">Телефон</label>
                <div class="col-md-3">
                    <form:input path="phone" id="phone" cssClass="form-control"/>
                </div>
                <label for="address" class="col-md-2 control-label">Адрес</label>
                <div class="col-md-3">
                    <form:input path="address" id="address" cssClass="form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label for="department" class="col-md-2 control-label">Отделение</label>
                <div class="col-md-8">
                    <select name="department" id="department"  class="form-control" onchange="loadSpec()">
                        <option value="none">-- Отделение --</option>
                        <c:forEach items="${departments}" var="department">
                            <option value="${department.id}">${department.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="speciality" class="col-md-2 control-label">Специальность</label>
                <div class="col-md-8">
                    <form:select path="speciality" disabled="true" id="speciality"  cssClass="form-control">
                    </form:select>
                </div>
            </div>

            <div class="col-md-12">
                <h3 style="text-align: center;">Подробная информация об учебном заведении</h3>
            </div>

            <div class="form-group">
                <label for="institutionName" class="col-md-2 control-label">Название</label>
                <div class="col-md-8">
                    <form:input path="institutionName" id="institutionName" cssClass="form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label for="institutionAddress" class="col-md-2 control-label">Адрес</label>
                <div class="col-md-3">
                    <form:input path="institutionAddress" id="institutionAddress" cssClass="form-control"/>
                </div>

                <label for="institutionPhone" class="col-md-2 control-label">Телефон</label>
                <div class="col-md-3">
                    <form:input path="institutionPhone" id="institutionPhone" cssClass="form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label for="graduationDate" class="col-md-2 control-label">Дата окончания</label>
                <div class="col-md-3">
                    <form:input path="graduationDate" id="graduationDate" type="date" cssClass="form-control"/>
                </div>

                <label for="honours" class="col-md-2 control-label">Отличия</label>
                <div class="col-md-3">
                    <form:select path="honours" id="honours" cssClass="form-control">
                        <c:choose>
                            <c:when test="${applicantModel.honours == 'ЗМ'}">
                                <form:option value="ЗМ" selected="selected">ЗМ</form:option>
                                <form:option value="Нет">Нет</form:option>
                            </c:when>
                            <c:otherwise>
                                <form:option value="ЗМ">ЗМ</form:option>
                                <form:option value="Нет" selected="selected">Нет</form:option>
                            </c:otherwise>
                        </c:choose>
                    </form:select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-10">
                    <button type="submit" class="btn btn-success pull-right" onclick="saveApplicant()">Сохранить</button>
                </div>
            </div>
        </form:form>
    </div>
</div>
</div>
</body>
</html>
