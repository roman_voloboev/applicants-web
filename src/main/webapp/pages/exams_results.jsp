<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Аббитуриенты</title>
    <%@include file="include/head.jsp"%>
    <script type="text/javascript" src="../webres/js/examresults.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            loadExamsResultsData();
            loadAvgMarks();
            loadApplicants();
            loadSubjects();
        });
    </script>
</head>
<body>
<div class="mainmenu-wrapper">
    <div class="container">
        <nav class="mainmenu">
            <ul>
                <li>
                    <a href='<c:url value="/applicants"/>'>Абитуриенты </a>
                </li>
                <li class="active">
                    <a href='<c:url value="/examsresults"/>'>Результаты экзаменов</a>
                </li>
                <li>
                    <a href='<c:url value="/subjects"/>'>Предметы</a>
                </li>
                <li>
                    <a href='<c:url value="/specialities"/>'>Специальности</a>
                </li>
                <li>
                    <a href='<c:url value="/departments"/>'>Отделения</a>
                </li>
            </ul>
        </nav>
    </div>
</div>

<div class="container" style="margin-top: 15px;">

    <div class="col-md-12">
        <div class="panel panel-primary">
            <div style="height: 30px; " class="panel-heading">
                <div class="pull-left">
                            <span class="clickable add" data-container="body" title="Добавить">
								<i class="glyphicon glyphicon-plus"></i>
							</span>
                </div>
            </div>
            <div class="panel-body" style="display: none;">
                <label for="examresults-tables-search" class="control-label">Поиск</label>
                <input type="text" class="form-control col-md-5" id="examresults-tables-search" data-action="search" data-filters="#contracts-table" placeholder="Введите данные.." />
            </div>

            <div class="panel-add" style="display: none;">
                <form:form modelAttribute="examResultModel" id="examResultForm" cssClass="form-horizontal">
                    <div class="form-group">
                        <label for="applicant" class="col-md-3 control-label">Имя</label>
                        <div class="col-md-7">
                            <form:select path="applicant" id="applicant" cssClass="form-control">
                            </form:select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="subject" class="col-md-3 control-label">Предмет</label>
                        <div class="col-md-7">
                            <form:select path="subject" id="subject" cssClass="form-control">
                            </form:select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="mark" class="col-md-3 control-label">Оценка</label>
                        <div class="col-md-7">
                            <form:select path="mark" id="mark" cssClass="form-control">
                                <option selected disabled>Выберите из списка...</option>
                                <form:option value="2">2</form:option>
                                <form:option value="3">3</form:option>
                                <form:option value="4">4</form:option>
                                <form:option value="5">5</form:option>
                            </form:select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-offset-3 col-md-7">
                            <button type="submit" class="btn btn-success" onclick="saveExamResult()">Добавить</button>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="table-responsive">
            <table style="font-size: 13px; "  class="table table-striped custab" id="examresults-table" cellspacing="0" cellpadding="0">
                <thead>
                <tr>
                    <th class="text-left">#</th>
                    <th>Аббитуриент</th>
                    <th>Предмет</th>
                    <th>Оценка</th>
                    <th class="text-center"></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-md-6">
        <div class="table-responsive">
            <table style="font-size: 13px;"  class="table table-striped custab" id="avgmarks-table" cellspacing="0" cellpadding="0">
                <thead>
                <tr>
                    <th>Аббитуриент</th>
                    <th>Средний бал</th>
                    <th>Зачислен</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>


</div>
</body>
</html>
