<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="../webres/css/bootstrap.css" rel="stylesheet">
<link href="../webres/css/main.css" rel="stylesheet">
<link href="../webres/css/font-awesome.css" rel="stylesheet">

<script src="../webres/js/jquery-1.11.1.min.js"></script>
<script src="../webres/js/bootstrap.min.js"></script>
<script src="../webres/js/custom.js"></script>
<script src="../webres/js/jQuery.print.js"></script>
