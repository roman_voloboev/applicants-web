<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Аббитуриенты</title>
    <%@include file="include/head.jsp"%>
    <script type="text/javascript" src="../webres/js/index.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            loadApplicantsData();
            loadDepData();
        });
    </script>
</head>
<body>
<div class="mainmenu-wrapper">
    <div class="container">
        <nav class="mainmenu">
            <ul>
                <li class="active">
                    <a href='<c:url value="/applicants"/>'>Абитуриенты </a>
                </li>
                <li>
                    <a href='<c:url value="/examsresults"/>'>Результаты экзаменов</a>
                </li>
                <li>
                    <a href='<c:url value="/subjects"/>'>Предметы</a>
                </li>
                <li>
                    <a href='<c:url value="/specialities"/>'>Специальности</a>
                </li>
                <li>
                    <a href='<c:url value="/departments"/>'>Отделения</a>
                </li>
            </ul>
        </nav>
    </div>
</div>

<div class="container" style="margin-top: 15px;">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div style="height: 30px;" class="panel-heading">
                <div class="pull-left">
                            <span class="clickable add" data-container="body" title="Добавить">
								<i class="glyphicon glyphicon-plus"></i>
							</span>
							<span style="margin-left: 15px;" class="clickable search" data-toggle="tooltip_search" title="Поиск" data-container="body">
								<i class="glyphicon glyphicon-search"></i>
							</span>
                            <span style="margin-left: 15px;" class="clickable" onclick="$.print('#applicants-table');" title="Распечатать">
                                <i class="glyphicon glyphicon-print"></i>
                            </span>
                </div>

                <ul class="nav navbar-right" style="padding-left: 30px;">
                    <li class="dropdown">
                        <a style="padding: 0; margin-top: -5px; color: #ffffff;" class="dropdown-toggle" role="button" data-toggle="dropdown" href="#">
                            <i class="glyphicon glyphicon-filter"></i> Фильтрация<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#" onclick="sortByHon()">По отличиям</a></li>
                        </ul>
                    </li>
                </ul>

                <ul class="nav navbar-right">
                    <li class="dropdown">
                        <a style="padding: 0; margin-top: -5px; color: #ffffff;" class="dropdown-toggle" role="button" data-toggle="dropdown" href="#">
                            <i class="glyphicon glyphicon-sort"></i> Сортировка<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#" onclick="sortBySpec()">По специальностям</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="panel-body" style="display: none;">
                <div class="col-md-6">
                    <input type="text" class="form-control col-md-6" id="applicants-table-search" data-action="search" data-filters="#applicants-table" placeholder="Введите имя аббитуриента.." />
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-success pull-left col-md-6" onclick="searchByName()">Поиск</button>
                </div>
            </div>

            <div class="panel-add" style="display: none;">
                <form:form modelAttribute="applicantModel" id="applicantForm" cssClass="form-horizontal">
                    <div class="form-group">
                        <div class="col-md-2">
                            <label for="name" class="control-label pull-right">ФИО</label>
                        </div>
                        <div class="col-md-8">
                            <form:input path="name" id="name" cssClass="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="birthday" class="col-md-2 control-label">Дата рождения</label>
                        <div class="col-md-3">
                            <form:input path="birthday" id="birthday" type="date" cssClass="form-control"/>
                        </div>

                        <label for="sex" class="col-md-2 control-label">Пол</label>
                        <div class="col-md-3">
                            <form:select path="sex" id="sex" cssClass="form-control">
                                <option selected disabled>Выберите из списка...</option>
                                <form:option value="М">М</form:option>
                                <form:option value="Ж">Ж</form:option>
                            </form:select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="phone" class="col-md-2 control-label">Телефон</label>
                        <div class="col-md-3">
                            <form:input path="phone" id="phone" cssClass="form-control"/>
                        </div>
                        <label for="address" class="col-md-2 control-label">Адрес</label>
                        <div class="col-md-3">
                            <form:input path="address" id="address" cssClass="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="department" class="col-md-2 control-label">Отделение</label>
                        <div class="col-md-8">
                            <select name="department" id="department"  class="form-control" onchange="loadSpec()">
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="speciality" class="col-md-2 control-label">Специальность</label>
                        <div class="col-md-8">
                            <form:select path="speciality" disabled="true" id="speciality"  cssClass="form-control">
                            </form:select>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <h3 style="text-align: center;">Подробная информация об учебном заведении</h3>
                    </div>

                    <div class="form-group">
                        <label for="institutionName" class="col-md-2 control-label">Название</label>
                        <div class="col-md-8">
                            <form:input path="institutionName" id="institutionName" cssClass="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="institutionAddress" class="col-md-2 control-label">Адрес</label>
                        <div class="col-md-3">
                            <form:input path="institutionAddress" id="institutionAddress" cssClass="form-control"/>
                        </div>

                        <label for="institutionPhone" class="col-md-2 control-label">Телефон</label>
                        <div class="col-md-3">
                            <form:input path="institutionPhone" id="institutionPhone" cssClass="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="graduationDate" class="col-md-2 control-label">Дата окончания</label>
                        <div class="col-md-3">
                            <form:input path="graduationDate" id="graduationDate" type="date" cssClass="form-control"/>
                        </div>

                        <label for="honours" class="col-md-2 control-label">Отличия</label>
                        <div class="col-md-3">
                            <form:select path="honours" id="honours" cssClass="form-control">
                                <option selected disabled>Выберите из списка...</option>
                                <form:option value="ЗМ">ЗМ</form:option>
                                <form:option value="Нет">Нет</form:option>
                            </form:select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-10">
                            <button type="submit" class="btn btn-success pull-right" onclick="saveApplicant()">Добавить</button>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="table-responsive">
            <table style="font-size: 12px;"  class="table table-striped custab" id="applicants-table" cellspacing="0" cellpadding="0">
                <thead>
                <tr>
                    <th class="text-left">#</th>
                    <th style="width: 10%">ФИО</th>
                    <th>Пол</th>
                    <th style="width: 8%">Д/Р</th>
                    <th style="width: 8%">Тел.</th>
                    <th style="width: 12%">Адрес</th>
                    <th>Спец.</th>
                    <th style="width: 10%">Назв. завед.</th>
                    <th style="width: 12%">Адрес</th>
                    <th>Телефон</th>
                    <th style="width: 8%">Д/О</th>
                    <th>Отличия</th>
                    <th style="width: 7%" class="text-center"></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>


</div>
</body>
</html>
