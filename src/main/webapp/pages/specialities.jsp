<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Аббитуриенты</title>
    <%@include file="include/head.jsp"%>
    <script type="text/javascript" src="../webres/js/specialities.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            loadSpecData();
            loadDepData();
        });
    </script>
</head>
<body>
<div class="mainmenu-wrapper">
    <div class="container">
        <nav class="mainmenu">
            <ul>
                <li>
                    <a href='<c:url value="/applicants"/>'>Абитуриенты </a>
                </li>
                <li>
                    <a href='<c:url value="/examsresults"/>'>Результаты экзаменов</a>
                </li>
                <li>
                    <a href='<c:url value="/subjects"/>'>Предметы</a>
                </li>
                <li class="active">
                    <a href='<c:url value="/specialities"/>'>Специальности</a>
                </li>
                <li>
                    <a href='<c:url value="/departments"/>'>Отделения</a>
                </li>
            </ul>
        </nav>
    </div>
</div>

<div class="container" style="margin-top: 15px;">

    <div class="col-md-12">
        <div class="panel panel-primary">
            <div style="height: 30px;" class="panel-heading">
                <div class="pull-left">
                            <span class="clickable add" data-container="body" title="Добавить">
								<i class="glyphicon glyphicon-plus"></i>
							</span>
                </div>
            </div>
            <div class="panel-body" style="display: none;">
                <label for="speciality-tables-search" class="control-label">Поиск</label>
                <input type="text" class="form-control col-md-5" id="speciality-tables-search" data-action="search" data-filters="#speciality-table" placeholder="Введите данные.." />
            </div>

            <div class="panel-add" style="display: none;">
                <form:form modelAttribute="specialityModel" id="specialityForm" cssClass="form-horizontal">
                    <div class="form-group">
                        <label for="name" class="col-md-3 control-label">Название специальности</label>
                        <div class="col-md-7">
                            <form:input path="name" id="name" cssClass="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="department" class="col-md-3 control-label">Отделение</label>
                        <div class="col-md-7">
                            <form:select path="department" id="department" cssClass="form-control">
                            </form:select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="averageMark" class="col-md-3 control-label">Проходной бал</label>
                        <div class="col-md-7">
                            <form:input path="averageMark" id="averageMark" cssClass="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-offset-3 col-md-7">
                            <button type="submit" class="btn btn-success" onclick="saveSpec()">Добавить</button>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="table-responsive">
            <table style="font-size: 13px;"  class="table table-striped custab" id="speciality-table" cellspacing="0" cellpadding="0">
                <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th>Название специальности</th>
                    <th>Название отделения</th>
                    <th>Проходной бал</th>
                    <th class="text-center"></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
