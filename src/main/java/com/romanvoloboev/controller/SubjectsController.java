package com.romanvoloboev.controller;

import com.romanvoloboev.entity.Subject;
import com.romanvoloboev.model.SubjectModel;
import com.romanvoloboev.service.SubjectBOImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

/**
 * @author Roman Voloboev
 */
@Controller
public class SubjectsController {

    @Autowired
    private SubjectBOImpl subjectBO;

    @RequestMapping(value = "/subjects")
    public ModelAndView showSubjPage(){
        ModelAndView modelAndView = new ModelAndView("subjects");
        modelAndView.addObject("subjectModel", new SubjectModel());
        return modelAndView;
    }

    @RequestMapping(value = "/subjects/load")
    @ResponseBody
    List<SubjectModel> loadSubjectsModelList() throws Exception {
        return subjectBO.selectModelListByEntities(subjectBO.selectAll());
    }

    @RequestMapping(value = "/subjects/save")
    @ResponseBody
    public ModelAndView saveSubject(@ModelAttribute("subjectModel")SubjectModel subjects) throws Exception {
        subjectBO.saveByModel(subjects);
        return new ModelAndView(new RedirectView("/subjects"));
    }

    @RequestMapping(value = "/subjects/delete")
    public ModelAndView deleteSubject(@RequestParam("id")Integer id) throws Exception {
        Subject subject = subjectBO.selectEntityById(id);
        subjectBO.delete(subject);
        return new ModelAndView(new RedirectView("/subjects"));
    }

}
