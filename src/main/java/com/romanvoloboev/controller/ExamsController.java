package com.romanvoloboev.controller;

import com.romanvoloboev.bo.ExamResultBO;
import com.romanvoloboev.model.AvgMarksModel;
import com.romanvoloboev.model.ExamResultModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

/**
 * @author Roman Voloboev
 */
@Controller
public class ExamsController {

    @Autowired private ExamResultBO examResultBO;

    @RequestMapping(value = "/examsresults")
    public ModelAndView showExamsResults() {
        ModelAndView modelAndView = new ModelAndView("exams_results");
        modelAndView.addObject("examResultModel", new ExamResultModel());
        return modelAndView;
    }

    @RequestMapping(value = "/examsresults/load")
    @ResponseBody
    public List<ExamResultModel> loadExamsData() throws Exception {
        return examResultBO.selectModelListByEntities(examResultBO.selectAll());
    }

    @RequestMapping(value = "/examsresults/save")
    public ModelAndView saveExamRes(@ModelAttribute("examresultModel") ExamResultModel examResultModel) throws Exception {
        examResultBO.saveByModel(examResultModel);
        return new ModelAndView(new RedirectView("/examsresults"));
    }

    @RequestMapping(value = "/examsresults/delete")
    public ModelAndView deleteExamRes(@RequestParam("id") Integer id) throws Exception {
        examResultBO.delete(examResultBO.selectEntityById(id));
        return new ModelAndView(new RedirectView("/examsresults"));
    }

    @RequestMapping(value = "/examsresults/load_avg_marks")
    @ResponseBody
    public List<AvgMarksModel> loadAvg() throws Exception {
        return examResultBO.selectAvg();
    }
}
