package com.romanvoloboev.controller;

import com.romanvoloboev.bo.ApplicantBO;
import com.romanvoloboev.bo.DepartmentBO;
import com.romanvoloboev.bo.SpecialityBO;
import com.romanvoloboev.entity.Applicant;
import com.romanvoloboev.model.ApplicantModel;
import com.romanvoloboev.model.DepartmentModel;
import com.romanvoloboev.model.SpecialityModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

/**
 * @author Roman Voloboev
 */
@Controller
public class ApplicantsController {

    @Autowired private ApplicantBO applicantBO;
    @Autowired private SpecialityBO specialityBO;
    @Autowired private DepartmentBO departmentBO;

    @RequestMapping(value = "/")
    public String redirectToMain(){
        return "redirect:/applicants";
    }

    @RequestMapping(value = "/applicants")
    public ModelAndView showIndexPage() throws Exception {
        ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("applicantModel", new ApplicantModel());
        modelAndView.addObject("specialityModel", specialityBO.selectAll());
        return modelAndView;
    }

    @RequestMapping(value = "/applicants/load")
    @ResponseBody
    public List<ApplicantModel> loadApplicants() throws Exception {
        return applicantBO.selectModelListByEntities(applicantBO.selectAll());
    }

    @RequestMapping(value = "/applicants/sort")
    @ResponseBody
    public List<ApplicantModel> loadApplicantsOrderBy(@RequestParam(value = "by") String orderBy) throws Exception {
        return applicantBO.selectModelListByEntities(applicantBO.selectAllOrderBy(orderBy));
    }

    @RequestMapping(value = "/applicants/save")
    public ModelAndView saveApplicant(@ModelAttribute("applicantModel")
                                     ApplicantModel applicantModel) throws Exception{
        applicantBO.saveByModel(applicantModel);
        return new ModelAndView(new RedirectView("/applicants"));
    }

    @RequestMapping(value = "/applicants/delete")
    public ModelAndView deleteApplicant(@RequestParam("id") Integer id) throws Exception {
        Applicant applicant = applicantBO.selectEntityById(id);
        applicantBO.delete(applicant);
        return new ModelAndView(new RedirectView("/applicants"));
    }

    @RequestMapping(value = "/applicants/edit")
    public ModelAndView editApplicant(@RequestParam("id") Integer id) throws Exception {
        ModelAndView modelAndView = new ModelAndView("edit_applicant");
        modelAndView.addObject("applicantModel", applicantBO.selectModelByEntity(applicantBO.selectEntityById(id)));
        modelAndView.addObject("specialities", specialityBO.selectModelListByEntities(specialityBO.selectAll()));
        modelAndView.addObject("departments", departmentBO.selectModelListByEntities(departmentBO.selectAll()));
        return modelAndView;
    }

    @RequestMapping(value = "/applicants/search")
    @ResponseBody
    public List<ApplicantModel> searchApplicant(@RequestParam("name") String name) throws Exception {
        return applicantBO.selectModelListByEntities(applicantBO.selectByName(name));
    }

}
