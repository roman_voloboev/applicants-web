package com.romanvoloboev.controller;

import com.romanvoloboev.bo.SpecialityBO;
import com.romanvoloboev.entity.Speciality;
import com.romanvoloboev.model.SpecialityModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

/**
 * @author Roman Voloboev
 */
@Controller
public class SpecialitiesController {

    @Autowired private SpecialityBO specialityBO;

    @RequestMapping(value = "/specialities")
    public ModelAndView showSpecPage(){
        ModelAndView modelAndView = new ModelAndView("specialities");
        modelAndView.addObject("specialityModel", new SpecialityModel());
        return modelAndView;
    }

    @RequestMapping(value = "/specialities/load")
    @ResponseBody
    List<SpecialityModel> loadSpecModelList() throws Exception {
        return specialityBO.selectModelListByEntities(specialityBO.selectAll());
    }

    @RequestMapping(value = "/specialities/save")
    public ModelAndView saveSpec(@ModelAttribute("specialityModel")SpecialityModel specialityModel) throws Exception {
        specialityBO.saveByModel(specialityModel);
        return new ModelAndView(new RedirectView("/specialities"));
    }

    @RequestMapping(value = "/specialities/load_by_dep")
    @ResponseBody
    public List<SpecialityModel> loadSpecModelListByDep(@RequestParam("id") Integer id) throws Exception {
        return specialityBO.selectModelListByDep(id);
    }

    @RequestMapping(value = "/specialities/delete")
    public ModelAndView deleteSpec(@RequestParam("id")Integer id) throws Exception {
        Speciality speciality = specialityBO.selectEntityById(id);
        specialityBO.delete(speciality);
        return new ModelAndView(new RedirectView("/specialities"));
    }
}
