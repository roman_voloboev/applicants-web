package com.romanvoloboev.controller;

import com.romanvoloboev.bo.DepartmentBO;
import com.romanvoloboev.model.DepartmentModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

/**
 * @author Roman Voloboev
 */
@Controller
public class DepartmentsController {

    @Autowired private DepartmentBO departmentBO;

    @RequestMapping(value = "/departments")
    public ModelAndView showDepPage() {
        ModelAndView modelAndView = new ModelAndView("departments");
        modelAndView.addObject("departmentModel", new DepartmentModel());
        return modelAndView;
    }

    @RequestMapping(value = "/departments/load")
    @ResponseBody
    public List<DepartmentModel> loadDepModel() throws Exception {
        return departmentBO.selectModelListByEntities(departmentBO.selectAll());
    }

    @RequestMapping(value = "/departments/delete")
    public ModelAndView deleteDep(@RequestParam("id") Integer id) throws Exception {
        departmentBO.delete(departmentBO.selectEntityById(id));
        return new ModelAndView(new RedirectView("/departments"));
    }

    @RequestMapping(value = "/departments/save")
    public ModelAndView saveDep(@ModelAttribute("departmentModel") DepartmentModel departmentModel) throws Exception {
        departmentBO.saveByModel(departmentModel);
        return new ModelAndView(new RedirectView("/departments"));
    }
}
