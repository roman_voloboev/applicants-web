package com.romanvoloboev.bo;

import com.romanvoloboev.entity.ExamResult;
import com.romanvoloboev.model.AvgMarksModel;
import com.romanvoloboev.model.ExamResultModel;

import java.util.List;

/**
 * @author Roman Voloboev
 */
public interface ExamResultBO {
    public void save(ExamResult o) throws Exception;
    public void update(ExamResult o) throws Exception;
    public void delete(ExamResult o) throws Exception;

    public void saveByModel(ExamResultModel examResultModel) throws Exception;
    public List<ExamResult> selectAll() throws Exception;
    public List<AvgMarksModel> selectAvg() throws Exception;
    public ExamResult selectEntityById(Integer id) throws Exception;
    public ExamResultModel selectModelByEntity(ExamResult examResult) throws Exception;
    public List<ExamResultModel> selectModelListByEntities(List<ExamResult> examResults) throws Exception;
}
