package com.romanvoloboev.bo;

import com.romanvoloboev.entity.Applicant;
import com.romanvoloboev.model.ApplicantModel;

import java.util.List;

/**
 * @author Roman Voloboev
 */
public interface ApplicantBO {
    public void save(Applicant o) throws Exception;
    public void update(Applicant o) throws Exception;
    public void delete(Applicant o) throws Exception;

    public void saveByModel(ApplicantModel applicantModel) throws Exception;
    public List<Applicant> selectAll() throws Exception;
    public Applicant selectEntityById(Integer id) throws Exception;
    public ApplicantModel selectModelByEntity(Applicant applicant) throws Exception;
    public List<ApplicantModel> selectModelListByEntities(List<Applicant> applicants) throws Exception;
    public List<Applicant> selectByName(String name) throws Exception;
    public List<Applicant> selectAllOrderBy(String orderBy) throws Exception;
}
