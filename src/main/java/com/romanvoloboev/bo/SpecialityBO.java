package com.romanvoloboev.bo;

import com.romanvoloboev.entity.Speciality;
import com.romanvoloboev.model.SpecialityModel;

import java.util.List;

/**
 * @author Roman Voloboev
 */
public interface SpecialityBO {
    public void save(Speciality o) throws Exception;
    public void update(Speciality o) throws Exception;
    public void delete(Speciality o) throws Exception;
    public void saveByModel(SpecialityModel specialityModel) throws Exception;
    public List<Speciality> selectAll() throws Exception;
    public Speciality selectEntityById(Integer id) throws Exception;
    public SpecialityModel selectModelByEntity(Speciality speciality) throws Exception;
    public List<SpecialityModel> selectModelListByEntities(List<Speciality> speciality) throws Exception;
    public List<SpecialityModel> selectModelListByDep(Integer id) throws Exception;
}