package com.romanvoloboev.bo;

import com.romanvoloboev.entity.Subject;
import com.romanvoloboev.model.SubjectModel;

import java.util.List;

/**
 * @author Roman Voloboev
 */
public interface SubjectBO {
    public void save(Subject o) throws Exception;
    public void update(Subject o) throws Exception;
    public void delete(Subject o) throws Exception;

    public void saveByModel(SubjectModel subjectModel) throws Exception;
    public List<Subject> selectAll() throws Exception;
    public Subject selectEntityById(Integer id) throws Exception;
    public SubjectModel selectModelByEntity(Subject subject) throws Exception;
    public List<SubjectModel> selectModelListByEntities(List<Subject> subjects) throws Exception;
}
