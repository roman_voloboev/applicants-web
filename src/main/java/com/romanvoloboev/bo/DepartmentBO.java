package com.romanvoloboev.bo;

import com.romanvoloboev.entity.Department;
import com.romanvoloboev.model.DepartmentModel;

import java.util.List;

/**
 * @author Roman Voloboev
 */
public interface DepartmentBO {
    public void save(Department o) throws Exception;
    public void update(Department o) throws Exception;
    public void delete(Department o) throws Exception;

    public void saveByModel(DepartmentModel departmentModel) throws Exception;
    public List<Department> selectAll() throws Exception;
    public Department selectEntityById(Integer id) throws Exception;
    public DepartmentModel selectModelByEntity(Department department) throws Exception;
    public List<DepartmentModel> selectModelListByEntities(List<Department> departments) throws Exception;
}
