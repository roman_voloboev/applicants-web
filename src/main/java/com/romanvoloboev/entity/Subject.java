package com.romanvoloboev.entity;

import javax.persistence.*;

/**
 * @author Roman Voloboev
 */

@NamedQueries({
        @NamedQuery(name = Subject.SELECT_BY_NAME, query = "SELECT s FROM Subject s WHERE LOWER(s.name) = :name")
})

@Entity
@Table(name = "subject")
public class Subject {
    public static final String SELECT_BY_NAME = "SELECT_SUBJECT_BY_NAME";

    private Integer id;
    private String name;

    public Subject() {
    }

    public Subject(String name) {
        this.name = name;
    }

    public Subject(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    @Id
    @SequenceGenerator(name = "sequence", sequenceName = "subject_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "sequence")
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
