package com.romanvoloboev.entity;

import javax.persistence.*;

/**
 * @author Roman Voloboev
 */

@NamedQueries({
        @NamedQuery(name = Speciality.SELECT_BY_DEPARTMENT, query = "SELECT s FROM Speciality s " +
                "INNER JOIN s.department d WHERE d.id = :departmentID")
})

@Entity
@Table(name = "speciality")
public class Speciality {
    public static final String SELECT_BY_DEPARTMENT = "SELECT_SPEC_BY_DEPARTMENT";

    private Integer id;
    private String name;
    private Department department;
    private Double avgSpecMark;

    public Speciality() {
    }

    public Speciality(Integer id, String name, Department department, Double avgSpecMark) {
        this.id = id;
        this.name = name;
        this.department = department;
        this.avgSpecMark = avgSpecMark;
    }

    public Speciality(String name, Department department, Double avgSpecMark) {
        this.name = name;
        this.department = department;
        this.avgSpecMark = avgSpecMark;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "sequence")
    @SequenceGenerator(name = "sequence", sequenceName = "speciality_seq")
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "name", length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "department")
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @Column(name = "avgSpecMark")
    public Double getAvgSpecMark() {
        return avgSpecMark;
    }

    public void setAvgSpecMark(Double avgSpecMark) {
        this.avgSpecMark = avgSpecMark;
    }
}
