package com.romanvoloboev.entity;

import javax.persistence.*;

/**
 * @author Roman Voloboev
 */

@NamedQueries({
        @NamedQuery(name = ExamResult.SELECT_AVG,
                query = "SELECT NEW com.romanvoloboev.model.AvgMarksModel(a.name, avg(e.mark), s.avgSpecMark)" +
                        " FROM ExamResult e INNER JOIN e.applicant a INNER JOIN a.speciality s GROUP BY a.name, s.avgSpecMark")
})

@Entity
@Table(name = "examresult")
public class ExamResult {
    public static final String SELECT_AVG = "SELECT_EXAMS_AVG";

    private Integer id;
    private Double mark;
    private Subject subject;
    private Applicant applicant;


    public ExamResult() {
    }

    public ExamResult(Integer id, Double mark, Subject subject, Applicant applicant) {
        this.id = id;
        this.mark = mark;
        this.subject = subject;
        this.applicant = applicant;
    }

    public ExamResult(Double mark, Subject subject, Applicant applicant) {
        this.mark = mark;
        this.subject = subject;
        this.applicant = applicant;
    }

    @Id
    @SequenceGenerator(name = "sequence", sequenceName = "exam_result_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "sequence")
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "mark")
    public Double getMark() {
        return mark;
    }

    public void setMark(Double mark) {
        this.mark = mark;
    }

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "subject")
    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "applicant")
    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }
}
