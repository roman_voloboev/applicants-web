package com.romanvoloboev.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Roman Voloboev
 */

@NamedQueries({
        @NamedQuery(name = Applicant.SELECT_BY_NAME, query = "SELECT a FROM Applicant a WHERE LOWER(a.name) like :name"),
        @NamedQuery(name = Applicant.SELECT_AND_ORDER_BY_SPEC, query = "SELECT a FROM Applicant a INNER JOIN a.speciality s ORDER BY s.name")
})

@Entity
@Table(name="applicant")
public class Applicant {
    public static final String SELECT_BY_NAME = "SELECT_APPLICANT_BY_NAME";
    public static final String SELECT_AND_ORDER_BY_SPEC = "SELECT_APPLICANT_ORDER_BY_SPEC";

    private Integer id;
    private String name;
    private String sex;
    private Date birthday;
    private String phone;
    private String address;
    private String honours;
    private Date graduationDate;
    private String institutionName;
    private String institutionAddress;
    private String institutionPhone;
    private Speciality speciality;

    public Applicant() {
    }

    public Applicant(Integer id, String name, String sex, Date birthday, String phone, String address,
                     String honours, Date graduationDate, String institutionName, String institutionAddress, String institutionPhone, Speciality speciality) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.birthday = birthday;
        this.phone = phone;
        this.address = address;
        this.honours = honours;
        this.graduationDate = graduationDate;
        this.institutionName = institutionName;
        this.institutionAddress = institutionAddress;
        this.institutionPhone = institutionPhone;
        this.speciality = speciality;
    }

    public Applicant(String name, String sex, Date birthday, String phone, String address,
                     String honours, Date graduationDate, String institutionName, String institutionAddress, String institutionPhone, Speciality speciality) {
        this.name = name;
        this.sex = sex;
        this.birthday = birthday;
        this.phone = phone;
        this.address = address;
        this.honours = honours;
        this.graduationDate = graduationDate;
        this.institutionName = institutionName;
        this.institutionAddress = institutionAddress;
        this.institutionPhone = institutionPhone;
        this.speciality = speciality;
    }

    @Id
    @SequenceGenerator(name = "sequence", sequenceName = "person_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "sequence")
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "name", length = 100, nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "sex", length = 10)
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Column(name = "birthday")
    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Column(name = "phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Column(name = "address", length = 100)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Column(name = "honours", length = 100)
    public String getHonours() {
        return honours;
    }

    public void setHonours(String honours) {
        this.honours = honours;
    }

    @Column(name = "graduationDate")
    public Date getGraduationDate() {
        return graduationDate;
    }

    public void setGraduationDate(Date graduationDate) {
        this.graduationDate = graduationDate;
    }

    @Column(name = "institutionName")
    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    @Column(name = "institutionAddress")
    public String getInstitutionAddress() {
        return institutionAddress;
    }

    public void setInstitutionAddress(String institutionAddress) {
        this.institutionAddress = institutionAddress;
    }

    @Column(name = "institutionPhone")
    public String getInstitutionPhone() {
        return institutionPhone;
    }

    public void setInstitutionPhone(String institutionPhone) {
        this.institutionPhone = institutionPhone;
    }

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "speciality")
    public Speciality getSpeciality() {
        return speciality;
    }

    public void setSpeciality(Speciality speciality) {
        this.speciality = speciality;
    }
}
