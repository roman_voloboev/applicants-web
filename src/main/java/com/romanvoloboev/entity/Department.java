package com.romanvoloboev.entity;

import javax.persistence.*;

/**
 * @author Roman Voloboev
 */

@NamedQueries({
        @NamedQuery(name = Department.SELECT_BY_NAME, query = "SELECT d FROM Department d WHERE LOWER(d.name) = :name")
})

@Entity
@Table(name = "department")
public class Department {
    public static final String SELECT_BY_NAME = "SELECT_DEPARTMENT_BY_NAME";

    private Integer id;
    private String name;

    public Department() {
    }

    public Department(String name) {
        this.name = name;
    }

    public Department(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    @Id
    @SequenceGenerator(name = "sequence", sequenceName = "department_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "sequence")
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
