package com.romanvoloboev.model;

/**
 * @author Roman Voloboev
 */
public class AvgMarksModel {
    private String name;
    private double avgApplicantMark;
    private double avgSpecMark;

    public AvgMarksModel(String name, double avgApplicantMark, double avgSpecMark) {
        this.name = name;
        this.avgApplicantMark = avgApplicantMark;
        this.avgSpecMark = avgSpecMark;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAvgApplicantMark() {
        return avgApplicantMark;
    }

    public void setAvgApplicantMark(double avgApplicantMark) {
        this.avgApplicantMark = avgApplicantMark;
    }

    public double getAvgSpecMark() {
        return avgSpecMark;
    }

    public void setAvgSpecMark(double avgSpecMark) {
        this.avgSpecMark = avgSpecMark;
    }
}


