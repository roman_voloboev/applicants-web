package com.romanvoloboev.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Roman Voloboev
 */
public class ExamResultModel {
    private int id;
    private double mark;
    private String subject;
    private String applicant;

    public ExamResultModel() {
    }

    public ExamResultModel(int id, double mark, String subject, String applicant) {
        this.id = id;
        this.mark = mark;
        this.subject = subject;
        this.applicant = applicant;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NotNull
    @Size(min = 1, max = 2, message = "Incorrect mark format")
    public double getMark() {
        return mark;
    }

    public void setMark(double mark) {
        this.mark = mark;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getApplicant() {
        return applicant;
    }

    public void setApplicant(String applicant) {
        this.applicant = applicant;
    }
}
