package com.romanvoloboev.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Roman Voloboev
 */
public class SpecialityModel {
    private int id;
    private String name;
    private String department;
    private double averageMark;

    public SpecialityModel() {
    }

    public SpecialityModel(int id, String name, String department, double averageMark) {
        this.id = id;
        this.name = name;
        this.department = department;
        this.averageMark = averageMark;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public double getAverageMark() {
        return averageMark;
    }

    public void setAverageMark(double averageMark) {
        this.averageMark = averageMark;
    }
}
