package com.romanvoloboev.model;

import javax.validation.constraints.NotNull;

/**
 * @author Roman Voloboev
 */
public class ApplicantModel {
    private int id;
    private String name;
    private String sex;
    private String birthday;
    private String phone;
    private String address;
    private String honours;
    private String graduationDate;
    private String institutionName;
    private String institutionAddress;
    private String institutionPhone;
    private String speciality;

    public ApplicantModel() {
    }

    public ApplicantModel(int id, String name, String sex, String birthday, String phone,
                          String address, String honours, String graduationDate,  String institutionName,
                          String institutionAddress, String institutionPhone, String speciality) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.birthday = birthday;
        this.phone = phone;
        this.address = address;
        this.honours = honours;
        this.graduationDate = graduationDate;
        this.institutionName = institutionName;
        this.institutionAddress = institutionAddress;
        this.institutionPhone = institutionPhone;
        this.speciality = speciality;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    @NotNull
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @NotNull
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGraduationDate() {
        return graduationDate;
    }

    public void setGraduationDate(String graduationDate) {
        this.graduationDate = graduationDate;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getHonours() {
        return honours;
    }

    public void setHonours(String honours) {
        this.honours = honours;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getInstitutionAddress() {
        return institutionAddress;
    }

    public void setInstitutionAddress(String institutionAddress) {
        this.institutionAddress = institutionAddress;
    }

    public String getInstitutionPhone() {
        return institutionPhone;
    }

    public void setInstitutionPhone(String institutionPhone) {
        this.institutionPhone = institutionPhone;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }
}
