package com.romanvoloboev.model;


import javax.validation.constraints.NotNull;

/**
 * @author Roman Voloboev
 */

public class SubjectModel {
    private int id;
    private String name;

    public SubjectModel() {
    }

    public SubjectModel(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
