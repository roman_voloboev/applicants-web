package com.romanvoloboev.service;

import com.romanvoloboev.bo.ExamResultBO;
import com.romanvoloboev.dao.DAOImpl;
import com.romanvoloboev.entity.Applicant;
import com.romanvoloboev.entity.ExamResult;
import com.romanvoloboev.entity.Subject;
import com.romanvoloboev.model.AvgMarksModel;
import com.romanvoloboev.model.ExamResultModel;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Roman Voloboev
 */
@Service
public class ExamResultBOImpl implements ExamResultBO {

    @Autowired private DAOImpl dao;
    @Autowired private SubjectBOImpl subjectBO;
    @Autowired private ApplicantBOImpl applicantBO;

    @Transactional
    @Override
    public void save(ExamResult o) throws Exception {
        dao.save(o);
    }

    @Transactional
     @Override
    public void update(ExamResult o) throws Exception {
        dao.update(o);
    }

    @Transactional
    @Override
    public void delete(ExamResult o) throws Exception {
        dao.delete(o);
    }

    @Transactional
    @Override
    public void saveByModel(ExamResultModel examResultModel) throws Exception {
            ExamResult examResult;
            Subject subject = null;
            Applicant applicant = null;

            try {
                subject = subjectBO.selectEntityById(Integer.valueOf(examResultModel.getSubject()));
                applicant = applicantBO.selectEntityById(Integer.valueOf(examResultModel.getApplicant()));
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (examResultModel.getId() ==0) {
                examResult = new ExamResult(examResultModel.getMark(), subject, applicant);
            }
            else {
                examResult = new ExamResult(examResultModel.getId(), examResultModel.getMark(), subject, applicant);
            }
            dao.saveOrUpdate(examResult);
    }

    @Transactional(readOnly = true)
    @Override
    public List<ExamResult> selectAll() throws Exception {
        return dao.getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery("from ExamResult").list();
    }

    @Transactional(readOnly = true)
    @Override
    public List<AvgMarksModel> selectAvg() throws Exception {
        return dao.getHibernateTemplate().getSessionFactory().getCurrentSession().getNamedQuery(ExamResult.SELECT_AVG).list();
    }

    @Transactional(readOnly = true)
    @Override
    public ExamResult selectEntityById(Integer id) throws Exception {
        return (ExamResult) dao.getHibernateTemplate().getSessionFactory().getCurrentSession().get(ExamResult.class, id);
    }

    @Override
    public ExamResultModel selectModelByEntity(ExamResult examResult) throws Exception {
        return new ExamResultModel(examResult.getId(), examResult.getMark(), examResult.getSubject().getName(), examResult.getApplicant().getName());
    }

    @Transactional(readOnly = true)
    @Override
    public List<ExamResultModel> selectModelListByEntities(List<ExamResult> examResults) throws Exception {
        List<ExamResultModel> examResultModels = new ArrayList<>();
        for (ExamResult e : examResults) {
            examResultModels.add(new ExamResultModel(e.getId(), e.getMark(), e.getSubject().getName(), e.getApplicant().getName()));
        }
        return examResultModels;
    }

    public int[] setApplicants(Collection<Applicant> applicants){
        int[] ids = new int[applicants.size()];
        int i = 0;
        for (Applicant applicant : applicants) {
            if (applicant != null) {
                ids[i++] = applicant.getId();
            }
        }
        return ids;
    }

    private List<Applicant> selectApplicants(int[] ids) throws Exception{
        List<Applicant> applicants = new ArrayList<>();
        for (int id : ids){
            Applicant applicant = applicantBO.selectEntityById(id);
            if (applicant != null) {
                applicants.add(applicant);
            }
        }
        return applicants;
    }
}
