package com.romanvoloboev.service;

import com.romanvoloboev.bo.SpecialityBO;
import com.romanvoloboev.dao.DAOImpl;
import com.romanvoloboev.entity.Department;
import com.romanvoloboev.entity.Speciality;
import com.romanvoloboev.model.SpecialityModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.xml.bind.ValidationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Roman Voloboev
 */
@Service
public class SpecialityBOImpl implements SpecialityBO {

    @Autowired private DAOImpl dao;
    @Autowired private DepartmentBOImpl departmentBO;

    @Transactional
    @Override
    public void save(Speciality o) throws Exception {
        dao.save(o);
    }

    @Transactional
    @Override
    public void update(Speciality o) throws Exception {
        dao.update(o);
    }

    @Transactional
    @Override
    public void delete(Speciality o) throws Exception {
        dao.delete(o);
    }

    @Transactional
    @Override
    public void saveByModel(SpecialityModel specialityModel) throws Exception {
        try {
            Speciality speciality;
            Department department = null;

            try {
                department = departmentBO.selectEntityById(Integer.valueOf(specialityModel.getDepartment()));
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(specialityModel.getId() == 0) {
                speciality = new Speciality(specialityModel.getName(), department, specialityModel.getAverageMark());
            }
            else {
                speciality = new Speciality(specialityModel.getId(), specialityModel.getName(), department, specialityModel.getAverageMark());
            }
            dao.saveOrUpdate(speciality);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional(readOnly = true)
    public List<Speciality> selectAll() throws Exception {
        return dao.getHibernateTemplate().getSessionFactory().getCurrentSession()
                .createQuery("from Speciality").list();
    }

    @Transactional(readOnly = true)
    @Override
    public Speciality selectEntityById(Integer id) throws Exception {
        return (Speciality) dao.getHibernateTemplate().getSessionFactory().getCurrentSession().get(Speciality.class, id);
    }

    @Override
    public SpecialityModel selectModelByEntity(Speciality speciality) throws Exception {
        return null;
    }

    @Transactional(readOnly = true)
    @Override
    public List<SpecialityModel> selectModelListByEntities(List<Speciality> speciality) throws Exception {
        List<SpecialityModel> specialityModels = new ArrayList<>();
        for (Speciality s : speciality) {
            specialityModels.add(new SpecialityModel(s.getId(), s.getName(), s.getDepartment().getName(), s.getAvgSpecMark()));
        }
        return specialityModels;
    }

    @Transactional(readOnly = true)
    @Override
    public List<SpecialityModel> selectModelListByDep(Integer id) throws Exception {
        return dao.getHibernateTemplate().getSessionFactory().getCurrentSession().
                getNamedQuery(Speciality.SELECT_BY_DEPARTMENT).setParameter("departmentID", id).list();
    }
}
