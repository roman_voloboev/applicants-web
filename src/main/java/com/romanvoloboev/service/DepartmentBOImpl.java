package com.romanvoloboev.service;

import com.romanvoloboev.bo.DepartmentBO;
import com.romanvoloboev.dao.DAOImpl;
import com.romanvoloboev.entity.Department;
import com.romanvoloboev.model.DepartmentModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Roman Voloboev
 */
@Service
public class DepartmentBOImpl implements DepartmentBO {

    @Autowired private DAOImpl dao;

    @Transactional
    @Override
    public void save(Department o) throws Exception {
        dao.save(o);
    }

    @Transactional
    @Override
    public void update(Department o) throws Exception {
        dao.update(o);
    }

    @Transactional
    @Override
    public void delete(Department o) throws Exception {
        dao.delete(o);
    }

    @Transactional
    @Override
    public void saveByModel(DepartmentModel departmentModel) throws Exception {
            Department department;
            if (departmentModel.getId() == 0) {
                department = new Department(departmentModel.getName());
            }
            else {
                department = new Department(departmentModel.getId(), departmentModel.getName());
            }
            dao.saveOrUpdate(department);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Department> selectAll() throws Exception {
        return dao.getHibernateTemplate().getSessionFactory().getCurrentSession()
                .createQuery("from Department").list();
    }

    @Transactional(readOnly = true)
    @Override
    public Department selectEntityById(Integer id) throws Exception {
        return (Department) dao.getHibernateTemplate().getSessionFactory().getCurrentSession().get(Department.class, id);
    }

    @Override
    public DepartmentModel selectModelByEntity(Department department) throws Exception {
        return null;
    }

    @Transactional(readOnly = true)
    @Override
    public List<DepartmentModel> selectModelListByEntities(List<Department> departments) throws Exception {
        List<DepartmentModel> departmentModels = new ArrayList<>();
        for (Department d : departments) {
            departmentModels.add(new DepartmentModel(d.getId(), d.getName()));
        }
        return departmentModels;
    }
}
