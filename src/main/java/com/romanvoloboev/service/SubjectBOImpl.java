package com.romanvoloboev.service;

import com.romanvoloboev.bo.SubjectBO;
import com.romanvoloboev.dao.DAOImpl;
import com.romanvoloboev.entity.Subject;
import com.romanvoloboev.model.SubjectModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.xml.bind.ValidationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Roman Voloboev
 */

@Service
public class SubjectBOImpl implements SubjectBO {

    @Autowired
    private DAOImpl dao;

    @Transactional
    @Override
    public void save(Subject o) throws Exception {
        dao.save(o);
    }

    @Transactional
    @Override
    public void update(Subject o) throws Exception {
        dao.update(o);
    }

    @Transactional
    @Override
    public void delete(Subject o) throws Exception {
        dao.delete(o);
    }

    @Transactional
    @Override
    public void saveByModel(SubjectModel subjectModel) throws Exception {
            Subject subject;
            if(subjectModel.getId() == 0) {
                subject = new Subject(subjectModel.getName());
            }
            else {
                subject = new Subject(subjectModel.getId(), subjectModel.getName());
            }
            dao.saveOrUpdate(subject);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Subject> selectAll() throws Exception {
        return dao.getHibernateTemplate().getSessionFactory().getCurrentSession()
                .createQuery("from Subject").list();
    }

    @Transactional(readOnly = true)
    @Override
    public Subject selectEntityById(Integer id) throws Exception {
        return (Subject) dao.getHibernateTemplate().getSessionFactory().getCurrentSession().get(Subject.class, id);
    }

    @Transactional
    @Override
    public SubjectModel selectModelByEntity(Subject subject) throws Exception {
        return null;
    }

    @Transactional
    @Override
    public List<SubjectModel> selectModelListByEntities(List<Subject> subjects) throws Exception {
        List<SubjectModel> subjectModels = new ArrayList<>();
        for (Subject s : subjects) {
            subjectModels.add(new SubjectModel(s.getId(), s.getName()));
        }
        return subjectModels;
    }
}
