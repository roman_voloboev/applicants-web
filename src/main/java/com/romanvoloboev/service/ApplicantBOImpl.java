package com.romanvoloboev.service;

import com.romanvoloboev.bo.ApplicantBO;
import com.romanvoloboev.dao.DAOImpl;
import com.romanvoloboev.entity.Applicant;
import com.romanvoloboev.entity.Speciality;
import com.romanvoloboev.model.ApplicantModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Roman Voloboev
 */
@Service
public class ApplicantBOImpl implements ApplicantBO {

    @Autowired private DAOImpl dao;
    @Autowired private SpecialityBOImpl specialityBO;

    @Transactional
    @Override
    public void save(Applicant o) throws Exception {
        dao.save(o);
    }

    @Transactional
    @Override
    public void update(Applicant o) throws Exception {
        dao.update(o);
    }

    @Transactional
    @Override
    public void delete(Applicant o) throws Exception {
        dao.delete(o);
    }

    @Transactional
    @Override
    public void saveByModel(ApplicantModel applicantModel) throws Exception {
                Applicant applicant;
                Speciality speciality = null;

                try {
                    speciality = specialityBO.selectEntityById(Integer.valueOf(applicantModel.getSpeciality()));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if(applicantModel.getId() == 0) {
                    applicant = new Applicant(applicantModel.getName(), applicantModel.getSex(),
                            formatDate(applicantModel.getBirthday()), applicantModel.getPhone(), applicantModel.getAddress(),
                            applicantModel.getHonours(), formatDate(applicantModel.getGraduationDate()),
                            applicantModel.getInstitutionName(),applicantModel.getInstitutionAddress(), applicantModel.getInstitutionPhone(), speciality);
                } else {
                    applicant = new Applicant(applicantModel.getId(), applicantModel.getName(), applicantModel.getSex(),
                            formatDate(applicantModel.getBirthday()), applicantModel.getPhone(), applicantModel.getAddress(),
                            applicantModel.getHonours(), formatDate(applicantModel.getGraduationDate()),
                            applicantModel.getInstitutionName(), applicantModel.getInstitutionAddress(), applicantModel.getInstitutionPhone(), speciality);
                }
                dao.saveOrUpdate(applicant);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Applicant> selectAll() throws Exception {
        return  dao.getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery("from Applicant").list();
    }

    @Transactional(readOnly = true)
    @Override
    public List<Applicant> selectAllOrderBy(String orderBy) throws Exception {
        switch (orderBy) {
            default:
                return dao.getHibernateTemplate().getSessionFactory().getCurrentSession().getNamedQuery(Applicant.SELECT_AND_ORDER_BY_SPEC).list();
            case "spec":
                return dao.getHibernateTemplate().getSessionFactory().getCurrentSession().getNamedQuery(Applicant.SELECT_AND_ORDER_BY_SPEC).list();
            case "honours":
                return dao.getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery("from Applicant a WHERE a.honours ='ЗМ'").list();
        }
    }

    @Transactional(readOnly = true)
    @Override
    public List<Applicant> selectByName(String q) throws Exception {
        return dao.getHibernateTemplate().getSessionFactory().getCurrentSession().getNamedQuery(Applicant.SELECT_BY_NAME)
                .setParameter("name", "%"+q.toLowerCase()+"%").list();
    }

    @Transactional(readOnly = true)
    @Override
    public Applicant selectEntityById(Integer id) throws Exception {
        return (Applicant) dao.getHibernateTemplate().getSessionFactory().getCurrentSession().get(Applicant.class, id);
    }

    @Override
    public ApplicantModel selectModelByEntity(Applicant applicant) throws Exception {
        return new ApplicantModel(applicant.getId(), applicant.getName(), applicant.getSex(), formatDate(applicant.getBirthday()),
                applicant.getPhone(), applicant.getAddress(), applicant.getHonours(), formatDate(applicant.getGraduationDate()),
                applicant.getInstitutionName(), applicant.getInstitutionAddress(), applicant.getInstitutionPhone(), applicant.getSpeciality().getName());
    }

    @Transactional(readOnly = true)
    @Override
    public List<ApplicantModel> selectModelListByEntities(List<Applicant> applicants) throws Exception {
        List<ApplicantModel> applicantModels = new ArrayList<>();
        for(Applicant a : applicants) {
            applicantModels.add(new ApplicantModel(a.getId(), a.getName(), a.getSex(),
                    formatDate(a.getBirthday()), a.getPhone(), a.getAddress(), a.getHonours(),
                    formatDate(a.getGraduationDate()), a.getInstitutionName(), a.getInstitutionAddress(), a.getInstitutionPhone(), a.getSpeciality().getName()));
        }
        return applicantModels;
    }

    public String formatDate(Date date) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        return dateFormat.format(date);
    }

    public Date formatDate(String date) throws ParseException{
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        return dateFormat.parse(date);
    }
}
