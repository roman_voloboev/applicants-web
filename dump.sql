--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: applicant; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE applicant (
    id integer NOT NULL,
    address character varying(100),
    birthday timestamp without time zone,
    graduationdate timestamp without time zone,
    honours character varying(100),
    institutionaddress character varying(255),
    institutionname character varying(255),
    institutionphone character varying(255),
    name character varying(100) NOT NULL,
    phone character varying(255),
    sex character varying(10),
    speciality integer
);


ALTER TABLE public.applicant OWNER TO postgres;

--
-- Name: department; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE department (
    id integer NOT NULL,
    name character varying(255)
);


ALTER TABLE public.department OWNER TO postgres;

--
-- Name: department_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE department_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.department_seq OWNER TO postgres;

--
-- Name: exam_result_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE exam_result_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.exam_result_seq OWNER TO postgres;

--
-- Name: examresult; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE examresult (
    id integer NOT NULL,
    mark double precision,
    applicant integer,
    subject integer
);


ALTER TABLE public.examresult OWNER TO postgres;

--
-- Name: person_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE person_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_seq OWNER TO postgres;

--
-- Name: speciality; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE speciality (
    id integer NOT NULL,
    avgspecmark double precision,
    name character varying(100),
    department integer
);


ALTER TABLE public.speciality OWNER TO postgres;

--
-- Name: speciality_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE speciality_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.speciality_seq OWNER TO postgres;

--
-- Name: subject; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE subject (
    id integer NOT NULL,
    name character varying(255)
);


ALTER TABLE public.subject OWNER TO postgres;

--
-- Name: subject_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE subject_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.subject_seq OWNER TO postgres;

--
-- Data for Name: applicant; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY applicant (id, address, birthday, graduationdate, honours, institutionaddress, institutionname, institutionphone, name, phone, sex, speciality) FROM stdin;
1	ул. Трубников, 11	1992-02-12 00:00:00	2014-05-30 00:00:00	ЗМ	ул. Харитонова, 2	КЗНСЗШ №7	12-11-22	Иванов И.И.	11-22-33	М	5
3	ул. Победы, 5	1993-07-21 00:00:00	2014-05-30 00:00:00	ЗМ	ул. Харитонова, 2	КЗНСЗШ №7	12-11-22	Сидоров К.Л.	88-44-71	М	2
2	ул. Грушко, 44	1993-07-15 00:00:00	2014-05-30 00:00:00	Нет	ул. Харитонова, 2	КЗНСЗШ №7	12-11-22	Петрова М.А.	88-99-17	Ж	1
\.


--
-- Data for Name: department; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY department (id, name) FROM stdin;
1	Транспортные технологии
2	Электромеханика
3	Радиотехника
4	Компьютерная и программная инженерия
5	Воздушный транспорт
\.


--
-- Name: department_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('department_seq', 5, true);


--
-- Name: exam_result_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('exam_result_seq', 6, true);


--
-- Data for Name: examresult; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY examresult (id, mark, applicant, subject) FROM stdin;
1	3	1	1
2	4	1	2
3	4	2	1
4	5	2	2
5	5	3	1
6	5	3	2
\.


--
-- Name: person_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('person_seq', 3, true);


--
-- Data for Name: speciality; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY speciality (id, avgspecmark, name, department) FROM stdin;
1	4.5	Организация авиационных перевозок	1
2	4.5	Эксплуатация авиационных комплексов	2
3	4.5	Техническая эксплуатация радиоэлектронного оборудования ВС	3
4	4.5	Техническая эксплуатация наземных средств радиоэлектронного обеспечения полетов	3
5	4.5	Разработка ПО	4
6	4.5	Обслуживание компьютерных систем и сетей	4
7	4.5	Техническое обслуживание ВС и двигателей	5
8	4.5	Техническое обслуживание средств хранения, транспортировки и заправки ГСМ	5
\.


--
-- Name: speciality_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('speciality_seq', 8, true);


--
-- Data for Name: subject; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY subject (id, name) FROM stdin;
1	Математика
2	Укр. яз.
\.


--
-- Name: subject_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('subject_seq', 2, true);


--
-- Name: applicant_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY applicant
    ADD CONSTRAINT applicant_pkey PRIMARY KEY (id);


--
-- Name: department_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY department
    ADD CONSTRAINT department_pkey PRIMARY KEY (id);


--
-- Name: examresult_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY examresult
    ADD CONSTRAINT examresult_pkey PRIMARY KEY (id);


--
-- Name: speciality_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY speciality
    ADD CONSTRAINT speciality_pkey PRIMARY KEY (id);


--
-- Name: subject_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY subject
    ADD CONSTRAINT subject_pkey PRIMARY KEY (id);


--
-- Name: fk_9osbr392yk9b7k9fcuhjvbwf2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY applicant
    ADD CONSTRAINT fk_9osbr392yk9b7k9fcuhjvbwf2 FOREIGN KEY (speciality) REFERENCES speciality(id);


--
-- Name: fk_mwkq9ahgljyoxupl9gmm8lhic; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY speciality
    ADD CONSTRAINT fk_mwkq9ahgljyoxupl9gmm8lhic FOREIGN KEY (department) REFERENCES department(id);


--
-- Name: fk_pk5hjcqb8raagb4h2l5ps9gn5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY examresult
    ADD CONSTRAINT fk_pk5hjcqb8raagb4h2l5ps9gn5 FOREIGN KEY (applicant) REFERENCES applicant(id);


--
-- Name: fk_qqx2stb7xj2wfuuiffknc86rr; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY examresult
    ADD CONSTRAINT fk_qqx2stb7xj2wfuuiffknc86rr FOREIGN KEY (subject) REFERENCES subject(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

